require "rails_helper"

feature "the signin process", js: true do
  # given(:target_name) { "David" }

  # background :each do
  #   # User.make(email: 'user@example.com', password: 'password')
  #   HighScore.create!(game: target_name, score: 20)
  # end

  scenario "signs me in" do
    visit '/high_scores'
    HighScore.create!(game: "David", score: 20)
    puts "HighScore.all: #{HighScore.all.inspect}"

    # poltergeist
    # puts "page.driver.headers: #{page.driver.headers.inspect}"
    # puts "page.driver.network_traffic.map(&:url): #{page.driver.network_traffic.map(&:url).inspect}"
    # puts "page.driver.network_traffic.map(&:method): #{page.driver.network_traffic.map(&:method).inspect}"
    # puts "page.driver.network_traffic.map(&:headers): #{page.driver.network_traffic.map(&:headers).inspect}"
    # puts "page.driver.network_traffic.map(&:time): #{page.driver.network_traffic.map(&:time).inspect}"
    # puts "page.driver.cookies: #{page.driver.cookies.inspect}"
    # save_screenshot('/Users/shinyamaeda/Documents/gitlab/sample-etag-cache/file.png')

    # Then tests can switch between using different browsers effortlessly:
    # Capybara.current_driver = :selenium_chrome

    expect(page).to have_content "David"
    expect(page).to have_content '20'
  end

  # given(:target_name) { "David" }

  # background :each do
  #   # User.make(email: 'user@example.com', password: 'password')
  #   HighScore.create!(game: target_name, score: 20)
  # end

  # scenario "signs me in" do
  #   puts "HighScore.all: #{HighScore.all.inspect}"
  #   visit '/high_scores'

  #   # poltergeist
  #   # puts "page.driver.headers: #{page.driver.headers.inspect}"
  #   # puts "page.driver.network_traffic.map(&:url): #{page.driver.network_traffic.map(&:url).inspect}"
  #   # puts "page.driver.network_traffic.map(&:method): #{page.driver.network_traffic.map(&:method).inspect}"
  #   # puts "page.driver.network_traffic.map(&:headers): #{page.driver.network_traffic.map(&:headers).inspect}"
  #   # puts "page.driver.network_traffic.map(&:time): #{page.driver.network_traffic.map(&:time).inspect}"
  #   # puts "page.driver.cookies: #{page.driver.cookies.inspect}"
  #   # save_screenshot('/Users/shinyamaeda/Documents/gitlab/sample-etag-cache/file.png')

  #   # Then tests can switch between using different browsers effortlessly:
  #   # Capybara.current_driver = :selenium_chrome

  #   expect(page).to have_content target_name
  #   expect(page).to have_content '20'
  # end
end
