require "rails_helper"

RSpec.describe HighScore, :type => :model do
  it "orders by score" do
    lindeman = HighScore.create!(game: "Andy", score: 10)
    chelimsky = HighScore.create!(game: "David", score: 20)

    expect(HighScore.all.order(score: :desc)).to eq([chelimsky, lindeman])
  end
end
