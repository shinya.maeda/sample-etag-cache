class EtagCaching
  def initialize app
    @app = app
  end

  def call env
    puts "#{self.class.name} - #{__callee__}: env['PATH_INFO']: #{env['PATH_INFO'].inspect}"
    return @app.call(env) unless env['PATH_INFO'] =~ /\A\/high_scores\z/

    # etag, cached_value_present = get_etag(env)
    etag = SecureRandom.hex
    if_none_match = env['HTTP_IF_NONE_MATCH']

    puts "#{self.class.name} - #{__callee__}: Rails.env: #{Rails.env}"
    puts "#{self.class.name} - #{__callee__}: env['HTTP_IF_NONE_MATCH']: #{env['HTTP_IF_NONE_MATCH'].inspect}"
    puts "#{self.class.name} - #{__callee__}: env['HTTP_IF_MODIFIED_SINCE']: #{env['HTTP_IF_MODIFIED_SINCE'].inspect}"
    if if_none_match == etag
      # Cached
      [304, { 'ETag' => etag }, []]
    else
      # Fresh
      status, headers, body = @app.call(env)
      headers['ETag'] = etag
      puts "#{self.class.name} - #{__callee__}: returned status: #{status}"
      puts "#{self.class.name} - #{__callee__}: returned headers: #{headers}"
      [status, headers, body]
    end
  end

  # def get_etag(env)
  #   cache_key = env['PATH_INFO']
  #   store = Gitlab::EtagCaching::Store.new
  #   current_value = store.get(cache_key)
  #   cached_value_present = current_value.present?

  #   unless cached_value_present
  #     current_value = store.touch(cache_key, only_if_missing: true)
  #   end

  #   [weak_etag_format(current_value), cached_value_present]
  # end
end
