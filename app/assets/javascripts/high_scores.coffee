# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

reload2 = () ->
	$.get '/high_scores', (data) ->
  	$("#score_table").html(data)
  console.log('called high_scores!!!')

preparing = () ->
	setInterval () ->
	  reload2()
	, 3000

$(document).on 'turbolinks:load', ->
	preparing()
